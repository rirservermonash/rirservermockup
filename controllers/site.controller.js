const csv = require('csv-parser');
const fs = require('fs');
const Global = require('../lib/global');
const Namespaces = require('../lib/namespaces');

// Load the db models to be used in this controller
const SiteModel = require('../models/site.model');

function apiResponse(message, errors, content, ) {
    return {
        message: message,
        errors: errors,
        content: content
    }
}

//Read Site Controller
exports.ShowSite = function(req, res) {
    let tenant = Namespaces.getCurrentTenantId()
    SiteModel.SiteModel().findOne({}, function(err, site) {
        console.log("GET SITE: ");
        console.log(site);
        if (err) {
            console.log("ERR ", err);
            res.send(Global.apiResponse('Unable to retrieve site details', [err.message], []));
        } else if (!site) {
            console.log("SITE NOT FOUND");
            res.send(Global.apiResponse('Unable to retrieve site details', ['Site details not found'], []));
        } else {
            let result = {}
            result.tag = site.tag;
            result.label = site.label;
            result.description = site.description;
            result.terms = site.terms;
            result.href = 'http://' + req.headers.host + '/' + tenant
            result.recordsHref = 'http://' + req.headers.host + '/' + tenant + "/record"

            res.send(Global.apiResponse('Site details Retrieved', [], result));
        }
    });
};

//Create Site Controller
exports.CreateSite = function (req, res) {
    /**
     * Create new record in mongoDB
     */
    console.log("CREATE SITE REQUEST BODY ");
    console.log(req.body);
    if (req.body.tag && req.body.label) {
        let document = {
            tag: req.body.tag,
            label: req.body.label,
            description: req.body.description,
            terms: req.body.terms,
        };

        let site = new (SiteModel.SiteModel())(document);
        site.save(function (error) {
            if (error) {
                res.send(Global.apiResponse('Unable to create site details', [error.toString()], []));
            } else {
                res.send(Global.apiResponse('Site details created successfully', [], document));
            }
        });
    } else {
        res.send(Global.apiResponse('Unable to create site details', ['Site label and/or description missing'], []));
    }
};

