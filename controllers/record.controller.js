const csv = require('csv-parser');
const fs = require('fs');
const Global = require('../lib/global');
const Namespaces = require('../lib/namespaces');
const PDFDocument = require('pdfkit');
const spawn = require('child_process').spawn;

const tmp = require('tmp');

// Load the db models to be used in this controller
const RecordModel = require('../models/record.model');
const SiteModel = require('../models/site.model');

function apiResponse(message, errors, content, ) {
    return {
        message: message,
        errors: errors,
        content: content
    }
}

//Simple test controller version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

//Default controller for root path
exports.default = function (req, res) {
    res.send('DEFAULT: MongoDB, Express, Rest, Node Server Stack!')
};

//Return list of records for subject
exports.ListRecords = function (req, res) {
    let subject = 'unknown subject';
    if (req.query.s) {
        subject = req.query.s.replace('20%', ' ');
    }

    console.log('Finding records for "' + subject + '" in ' +  Namespaces.getCurrentTenantId());

    let recordList = [];

    RecordModel.RecordModel().find({subject: subject}, 'label description category type date', function(err, records) {
        if (err) {
            res.send(apiResponse('Unable to retrieve record', [err.message], []));
        } else {
            records.forEach(function (record) {
                recordList.push(record);
            });

            let message = 'Record list result';
            if (recordList.length == 0) {
                message = 'No Matching records found';
            }

            res.send(apiResponse(message, [], recordList));
        }
    });


};

//Read Record Controller
exports.ShowRecord = function(req, res) {
    RecordModel.RecordModel().findById(req.params.id, function(err, record) {

        if (err) {
            res.send(apiResponse('Unable to retrieve record', [err.message], []));
        } else if (!record) {
            res.send(apiResponse('Unable to retrieve record', ['Record not found'], []));
        } else {
            res.send(apiResponse('Record Retrieved', [], record));
        }
    });
};

exports.ShowRecordAttachment = function(req, res) {
    let prefix = Global.DATA_PATH + '/' + Namespaces.getCurrentTenantId()

    if (process.platform === 'linux') {
        prefix = Global.LINUX_DATA_PATH + '/' + Namespaces.getCurrentTenantId()
    }

    let theSite = null
    SiteModel.SiteModel().findOne({}, function(err, site) {
       theSite = site
    });

    RecordModel.RecordModel().findById(req.params.id, function(err, record) {

        if (err) {
            res.send(apiResponse('Unable to retrieve record', [err.message], []));
        } else if (!record) {
            res.send(apiResponse('Unable to retrieve record', ['Record not found'], []));
        } else {
            //Create temp header pdf
            var headerTmpFile = tmp.fileSync({ prefix: 'header-', postfix: '.pdf' });
            let ws = fs.createWriteStream(headerTmpFile.name)

            let header = new PDFDocument({autoFirstPage: false});
            header.pipe(ws);

            header.addPage({size: 'A5'});
            // Embed a font, set the font size, and render some text
            header.fontSize(16).font('Times-Bold')
                .text(theSite.label + ': terms of use', 10, 10);

            header.moveDown

            header.fontSize(14).font('Times-Roman')
                .text(theSite.terms );

            // draw bounding rectangle
            header.rect(5, 5, 410, header.y).stroke();

            // Finalize PDF file
            header.end();

            ws.on('finish', function () {

                // prepend header to record file
                let outputFile = tmp.fileSync({ prefix: 'output-', postfix: '.pdf' })
                let prc = spawn('/usr/bin/pdftk',  [headerTmpFile.name, prefix + '/' + record.filename, 'output', outputFile.name]);

                prc.stdout.on('data', function (data) {
                    console.log(data);
                });

                prc.stderr.on('data', function (data) {
                    console.log('prc stderr: ' + data);
                });

                prc.on('close', function (code) {
                    res.download(outputFile.name, record.filename);
                });

            });

        }
    });
};

exports.BulkUpload = function(req, res) {
        fs.createReadStream(Global.DATA_PATH + '/' + Namespaces.getCurrentTenantId() + '/data.csv')
            .pipe(csv())
            .on('data', (row) => {
                if(row.filename) {
                    let record = new (RecordModel.RecordModel())(row);
                    record.save(function (error) {
                        if (error) {
                            throw error;
                        }
                    });
                }
            })
            .on('end', () => {
                res.send('Bulk upload successfull');
            });
}

/****************************************************************************
 * NOT WORKING BELOW HERE
 * @param req
 * @param res
 * @constructor
 ****************************************************************************/
//Controllers for CRUD apis
//Create Record Controller
exports.CreateRecord = function (req, res) {
    RecordModel().upload(req, res, (error) => {
        if(error) {
            res.send('ERROR: ' + error.toString());
        } else {
            /**
             * Create new record in mongoDB
             */
            let filename = "files/" + req.file.filename;
            let document = {
                filename:     filename,
                label: req.body.label,
                description: req.body.description,
                category: req.body.category,
                type: req.body.type,
                date: req.body.date,
                subject: req.body.subject,
            };

            let record = new RecordModel.RecordModel(document);
            record.save(function(error){
                if(error){
                    throw error;
                }
                res.send('Record uploaded successfully');
            });
        }

    });
    let record = new RecordModel.RecordModel(
        {
            name: req.body.name,
            content: req.body.content
        }
    );
    record.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Record Created successfully')
    })
};

//Default controller for root path
exports.DeleteRecord = function (req, res) {
    res.send('RECORD DELETE: MongoDB, Express, Rest, Node Server Stack!')
};

//Upload Photo Controller
exports.UploadPhoto = function(req, res) {
    RecordModel().upload(req, res, (error) => {
        if(error) {
            res.send('ERROR: ' + error.toString());
        } else {
            /**
             * Create new record in mongoDB
             */
            let fullPath = "files/" + req.file.filename;
            let document = {
                path:     fullPath,
                caption:   req.body.caption
            };
            let photo = new RecordModel.PhotoModel(document);
            photo.save(function(error){
                if(error){
                    throw error;
                }
                res.send('Photo uploaded successfully');
            });
        }

    });
};