//Simple test controller version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

//Default controller for root path
exports.default = function (req, res) {
    res.send('Rights in Records Mocker Server MongoDB, Express, Rest, Node Server Stack!')
};

//An extra comment