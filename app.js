const express = require('express');
const bodyParser = require('body-parser');
const Router = require('./lib/router');
const RecordModel = require('./models/record.model');
const Namespaces = require('./lib/namespaces');

// Start the connection to mongo database
RecordModel.db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Initiate the server
const app = express();

// parses incoming requests with JSON payloads
app.use(bodyParser.json());

// parses incoming requests with urlencoded payloads
app.use(bodyParser.urlencoded({extended: false}));

/*****************************************************************************
 * Multi Tenant Stuff
 *****************************************************************************/
app.use(Namespaces.bindCurrentNamespace);
app.use((req, res, next) => {
    // Get the tenant from the url i.e. /<tenant>/record...
    let tenantId = 'unknown'
    let match = req.originalUrl.match(/^\/(.+?)\//i)
    if (match) {
        tenantId = match[1]
    }
    Namespaces.setCurrentTenantId(tenantId);

    next();
});
/*****************************************************************************
 * End Multi Tenant Stuff
 *****************************************************************************/

// Mount custom middleware RecordRoute to /record path
// the middleware function is executed when the base of the requested path matches path
app.use('/', Router);

// Start the server at port 1234
const port = 1234;
app.listen(port, () => {
    console.log('Server is up and running at port ' + port);
});

/*
   Important links
   https://expressjs.com/en/api.html
   http://programmerblog.net/nodejs-file-upload-tutorial/
   https://stackoverflow.com/questions/16015548/tool-for-sending-multipart-form-data-request
   https://github.com/expressjs/multer/issues/203

   And for the multi-tenant stuff:
   http://nmajor.com/posts/multi-tenancy-with-expressmongoose
 */




