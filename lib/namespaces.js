// lib/storage.js
const CLS = require('continuation-local-storage');

const namespaceName = 'request';
const ns = CLS.createNamespace(namespaceName);

function bindCurrentNamespace(req, res, next) {
    ns.bindEmitter(req);
    ns.bindEmitter(res);

    ns.run(() => {
        next();
    });
}

function setCurrentTenantId(tenantId) {
    return ns.set('tenantId', tenantId);
}

function getCurrentTenantId() {
    return ns.get('tenantId');
}

module.exports.bindCurrentNamespace = bindCurrentNamespace;
module.exports.setCurrentTenantId = setCurrentTenantId;
module.exports.getCurrentTenantId = getCurrentTenantId;