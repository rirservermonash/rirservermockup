const express = require('express');
const router = express.Router();

/****************************************************************************************
 * Global APIs
 ****************************************************************************************/
const DefaultController = require('../controllers/default.controller');

// default root api
router.get('/', DefaultController.default);

// a simple test url to check that all of our files are communicating correctly.
router.get('/test', DefaultController.test);

/****************************************************************************************
 * Record API
 ****************************************************************************************/
// Require the controller, so that the roots can be linked to the functions of the controller
const RecordController = require('../controllers/record.controller');

router.get('/*/record', RecordController.ListRecords);
router.get('/*/record/:id', RecordController.ShowRecord);
router.get('/*/recordattachment/:id', RecordController.ShowRecordAttachment);
router.get('/*/bulkupload', RecordController.BulkUpload);

/****************************************************************************************
 * Site API
 ****************************************************************************************/
// Require the controller, so that the roots can be linked to the functions of the controller
const SiteController = require('../controllers/site.controller');

router.get('/*/site', SiteController.ShowSite);
router.post('/*/site', SiteController.CreateSite);

module.exports = router; // EXPORT Routes