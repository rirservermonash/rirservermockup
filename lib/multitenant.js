// lib/multiTenant.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Namespaces = require('./namespaces');

function tenantModel(name, schema, options) {
    return (props = {}) => {
        schema.add({ tenantId: String });
        const Model = mongoose.model(name, schema, options);

        const { skipTenant } = props;
        if (skipTenant) return Model;

        Model.schema.set('discriminatorKey', 'tenantId');

        const tenantId = Namespaces.getCurrentTenantId();
        const discriminatorName = `${Model.modelName}-${tenantId}`;
        const existingDiscriminator = (Model.discriminators || {})[discriminatorName];
        return existingDiscriminator || Model.discriminator(discriminatorName, new Schema({}));
    };
}

function tenantlessModel(name, schema, options) {
    return () => mongoose.model(name, schema, options);
}

module.exports.tenantModel = tenantModel;
module.exports.tenantlessModel = tenantlessModel;