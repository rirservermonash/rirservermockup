function apiResponse(message, errors, content, ) {
    return {
        message: message,
        errors: errors,
        content: content
    }
}

module.exports = Object.freeze({
    DATA_PATH: './files',
    LINUX_DATA_PATH: '/var/productionserver/source/files',
    DB_NAME: 'rirbd-mock-db',
    apiResponse : apiResponse
});
