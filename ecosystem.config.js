module.exports = {
  apps : [{
    name: 'API',
    script: 'app.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    },
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      host : '118.138.235.12',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:rirservermonash/rirservermockup.git',
      ssh_options: ["StrictHostKeyChecking=no", "PasswordAuthentication=no", "ForwardAgent=yes"],
      path : '/var/productionserver',
      "pre-setup": "sudo rm -rf /var/productionserver/source/",
      'post-deploy' : 'sudo npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
