const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MultiTenant = require('./../lib/multitenant');

// Define Records Schema and Generate the corresponding mongo db Model
let SiteSchema = new Schema(
    {
        tag:  {type: String, required: true},
        label: {type: String, required: true},
        description: {type: String, required: false},
        terms: {type: String, required: false},
    },
    {
        timestamps: true,
    },
);
module.exports.SiteModel = MultiTenant.tenantModel('Sites', SiteSchema, ); //EXPORT Sites Model
