const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const multer = require('multer');
const path = require('path');
const Global = require('../lib/global');
const Namespaces = require('./../lib/namespaces');
const MultiTenant = require('./../lib/multitenant');

// Define Records Schema and Generate the corresponding mongo db Model
let RecordSchema = new Schema(
    {
        label: {type: String, required: true},
        description: {type: String, required: false},
        category: {type: String, required: true},
        type: {type: String, required: true},
        date: {type: String, required: false},
        subject: {type: String, required: true},
        filename:  {type: String, required: false},
    },
    {
        timestamps: true
    },
);
module.exports.RecordModel = MultiTenant.tenantModel('Records', RecordSchema, ); //EXPORT Records Model

/** Storage Engine */
const storageEngine = multer.diskStorage({
    destination: function(req, file, fn){
        fn(null,  Global.DATA_PATH + '/' + Namespaces.getCurrentTenantId())
    },
    filename: function(req, file, fn){
        fn(null,  new Date().getTime().toString()+'-'+file.fieldname+path.extname(file.originalname));
    }
});

//init
const upload =  multer({
    storage: storageEngine,
    limits: { fileSize:16000000 },//16MB: this is also the limit of MongoDB
    fileFilter: function(req, file, callback){
        validateFile(file, callback);
    }
}).single('photo');

let validateFile = function(file, cb ){
    allowedFileTypes = /jpeg|jpg|png|gif|pdf|docx/;
    const extension = allowedFileTypes.test(path.extname(file.originalname).toLowerCase());
    const mimeType  = allowedFileTypes.test(file.mimetype);
    if(extension && mimeType){
        return cb(null, true);
    }else{
        cb("Invalid file type. Only PDF, DOCX, JPEG, PNG and GIF file are allowed.")
    }
};

module.exports.upload = upload;


// Connect to the database mydb created in mongodb running at port 27017
console.log('Connecting to mydb database in mongodb');
mongoose
    .connect('mongodb://127.0.0.1:27017/' + Global.DB_NAME, { useNewUrlParser: true })
    .catch(e => {
        console.error('Connection error', e.message)
    });
console.log('Database connected successfully');
const db = mongoose.connection;
module.exports.db = db;
